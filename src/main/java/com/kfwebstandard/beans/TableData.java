package com.kfwebstandard.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import javax.enterprise.context.SessionScoped;

import javax.inject.Named;

/**
 * Class that provides data for the table In practice this would likely be from
 * a db. Supports editing of the bean
 *
 * @author Ken Fogel
 */
@Named
@SessionScoped
public class TableData implements Serializable {

    private final ArrayList<Name> names = new ArrayList<>(Arrays.asList(
            new Name("Bruce", "Wayne"), new Name("Clark", "Kent"),
            new Name("Hal", "Jordan"), new Name("Diana", "Prince")));

    public ArrayList<Name> getNames() {
        return names;
    }

    public String save() {
        names.stream().forEach((name) -> {
            name.setEditable(false);
        });
        // Returns null so that the page remains
        return null;
    }

}
