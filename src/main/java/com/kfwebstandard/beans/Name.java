package com.kfwebstandard.beans;

import java.io.Serializable;

/**
 * A bean that represents the names to be displayed in the table
 *
 * @author Ken Fogel
 */
public class Name implements Serializable {

    private String first;
    private String last;
    private boolean editable;

    public Name(String first, String last) {
        this.first = first;
        this.last = last;
    }

    public void setFirst(String newValue) {
        first = newValue;
    }

    public String getFirst() {
        return first;
    }

    public void setLast(String newValue) {
        last = newValue;
    }

    public String getLast() {
        return last;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean newValue) {
        editable = newValue;
    }
}
